#!/bin/bash

# files to delete
files_to_delete="CMakeCache.txt CMakeFiles cmake_install.cmake Makefile CMakeCache.txt CTestTestfile.cmake"


# delete the files or directories
for i in $files_to_delete; do
	#echo $i
	# delete dirs
	if [ -d "$i" ]; then
		rm -rf $i
	fi

	# delete files
	if [ -f "$i" ]; then
		rm  $i
	fi
done






