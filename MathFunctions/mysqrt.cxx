//#include "mysqrt.h"
#include "math.h"
#include "stdio.h"

short isqrt(short num) {
	short res = 0;
	short bit = 1 << 14; // The second to top bit set: 1 << 30 for 32 bits

	// "bit" starts at the highest power of four <= the argument
	while (bit > num) {
		bit >>= 2;
	}

	while (bit != 0) {
		if (num >= res + bit) {
			num -= res + bit;
			res = (res >> 1) + bit;
		} else {
			res >>= 1;
		}
		bit >>= 2;
	}
	return res;
}


double mysqrt(double inputValue) {
	double result;
//	printf("mysqrt() called from the library code\n");

// If we have both log and exp then use them
// From the page below, I've added some of the code based upon
//
// https://en.wikipedia.org/wiki/Methods_of_computing_square_roots

#if defined (HAVE_LOG) && defined (HAVE_EXP)
	// Exponential identigy
	result = exp(log(inputValue)*0.5);  // Raise e to 1/2 natural log of S

#else // otherwise use an iterative approach
	// he says use an iterative approach, but does not say
	result = sqrt(inputValue);
#endif
	return result;
}
