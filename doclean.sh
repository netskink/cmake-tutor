#!/bin/bash

# files to delete
files_to_delete="CMakeCache.txt \
CMakeFiles \
TutorialConfig.h \
cmake_install.cmake \
Makefile \
CMakeCache.txt \
CTestTestfile.cmake \
DartConfiguration.tcl \
Testing \
install_manifest.txt \
include \
bin \
Tutorial-1.0.1-Linux.tar.gz \
Tutorial-1.0.1-Source.tar.bz2 \
Tutorial-1.0.1-Source.tar.gz \
Tutorial-1.0.1-Linux.tar.gz  \
Tutorial-1.0.1-Source.tar.gz \
Tutorial-1.0.1-Linux.tar.Z \
Tutorial-1.0.1-Source.tar.xz \
Tutorial-1.0.1-Source.tar.Z \
CPackConfig.cmake \
CPackSourceConfig.cmake \
Tutorial-1.0.1-Linux.sh \
_CPack_Packages \
"


# delete the files or directories
for i in $files_to_delete; do
	#echo $i
	# delete dirs
	if [ -d "$i" ]; then
		rm -rf $i
	fi

	# delete files
	if [ -f "$i" ]; then
		rm $i
	fi
done






# do the lower subdir
cd MathFunctions
./doclean.sh
cd ..

